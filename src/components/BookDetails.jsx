// Importation des hooks useState et useEffect
import { useState, useEffect } from "react";
import { Link, useParams } from 'react-router-dom';
import { getBookDetails } from "./OpenLibraryService";
import PropTypes from "prop-types";
import "./BookDetails.css";

// Définition du composant BookDetails
function BookDetails() {
  //recup la parametre de l url qui à été declaré dans le router
  const { bookId } = useParams();
  // Déclaration d'un état 'book' et de sa fonction setter 'setBook', initialisée à null
  const [book, setBook] = useState(null);
  //console.log('new bookdetail for ' + bookId);
  // Utilisation du hook useEffect pour exécuter une action après le rendu initial et à chaque mise à jour de bookId ou setbooks
  useEffect(() => {
    const getDetails = async () => {
      console.log("getdetails: " + bookId);
      try {
        // Appel de la fonction getBookDetails pour récupérer les détails du livre en fonction de bookId
        const bookData = await getBookDetails(bookId);
        console.log(bookData);
        //à quoi sert setbooks (fonction)
        // Mise à jour de l'état 'books' avec les données des livres obtenues depuis la réponse
        setBook(bookData.data);
      } catch (error) {
        // Gestion des erreurs en cas de problème lors de la récupération des détails du livre
        console.error("Error fetching book details:", error);
      }
    };
    // Appel de la fonction getBookdetails à chaque changement de bookId ou setbooks
    getDetails();
  }, [bookId]);

  // Vérification si book est null ou undefined
  if (!book) {
    return <div>Loading...</div>;
  }

  // Vérification si book est non null et non undefined
  /*if (!book.title || !book.author || !book.description) {
    return <div>Missing book details</div>;
}*/

  // Rendu du composant BookDetails
  return (
    <div>
      <h1>{book.title}</h1>
      <h3>{book.first_publish_year || book.first_publish_date}</h3>
  {/*<p>{book.author_name}</p>*/}
      <p>{book.description.value || book.description}</p>
      <div className="link">
        <Link to="/">Back to Book List</Link>
      </div>
    </div>
  );
}

// Validation des props
BookDetails.propTypes = {
  bookId: PropTypes.string,
};

// Exportation du composant BookDetails pour une utilisation externe
export default BookDetails;
