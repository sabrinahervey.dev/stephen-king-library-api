import "./BookList.css";
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { getBookList } from './OpenLibraryService';
//import BookDetails from './BookDetails';

// Déclaration d'un état 'books' et de sa fonction setter 'setBooks', initialisée à un tableau vide
const BookList = () => {
  const [books, setBooks] = useState([]);

  // Utilisation du hook useEffect pour exécuter une action après le rendu initial
  useEffect(() => {
    const fetchBooks = async () => {  // Définition d'une fonction asynchrone fetchBooks pour récupérer la liste des livres
      console.log('fetchBooks');
      // Utilisation d'un bloc try...catch pour gérer les erreurs potentielles lors de la récupération des livres
      try {
        const response = await getBookList(); // Appel de la fonction getBookList pour récupérer la liste des livres
        console.log(response.data);
        setBooks(response.data.docs); // Mise à jour de l'état 'books' avec les données des livres obtenues depuis la réponse
      } catch (error) {
        console.error('Error fetching books:', error);
      }
    };

    // Appel de la fonction fetchBooks pour récupérer les livres une fois le composant monté
  // Utilisation de l'array vide comme dépendance pour exécuter le chargement une seule fois lors du montage du composant
  // Rendu du composant BookList
    fetchBooks();
  }, []);

  return (
  
    <div className="bloc"> 
      <a href="https://en.wikipedia.org/wiki/Stephen_King"><h1>Stephen King Books</h1></a>
      <ul>
        { books.map((book) => ( //Utilisation de la méthode map() pour parcourir chaque livre du tableau
          <li key={book.key} > {/*Chaque livre est placé dans un élément de liste (li) avec une clé unique */}
           <h4>{book.title}</h4>
           <Link to={`/BookDetails${book.key}`}>
           <img src={`https://covers.openlibrary.org/b/id/${book.cover_i}-M.jpg`} alt={book.title} />
           </Link>
            {/*<Link to={`/BookDetails${book.key}`}>{book.title}</Link>  {/* Utilisation de la balise Link pour créer un lien vers le détail du livre */}
          {/* <BookDetails bookId={book.key}/> */}  
          </li>
        ))}
      </ul>
    </div>
  );
};

// Exportation du composant BookList pour une utilisation externe
export default BookList;
