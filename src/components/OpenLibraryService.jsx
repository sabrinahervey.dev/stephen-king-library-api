// Importation de la bibliothèque Axios pour effectuer des requêtes HTTP
import axios from "axios";
// URL de base de l'API Open Library
const API_URL = 'https://openlibrary.org';

// Création d'un service Axios pour interagir avec l'API Open Library
// eslint-disable-next-line react-refresh/only-export-components
const LibraryService = axios.create({
// Définition de l'URL de base pour toutes les requêtes
   baseURL: API_URL,
// Définition de l'en-tête Content-Type pour les requêtes JSON
   headers: {
    'Content-Type': 'application/json',
   }, 
});

// Fonction pour récupérer la liste des livres de Stephen King
const getBookList = () => {
     // Appel de la méthode GET de LibraryService avec l'URL spécifique pour récupérer la liste des livres
    return LibraryService.get(`/search.json?author=stephen+king&page=1&limit=10`);
};

// Fonction pour récupérer les détails d'un livre
const getBookDetails = (bookKey) => {
// Appel de la méthode GET de LibraryService avec une URL dynamique pour récupérer les détails d'un livre spécifique
    return LibraryService.get(`/works/${bookKey}.json`);
};

// Exportation des fonctions pour les rendre disponibles pour une utilisation externe
export {getBookList, getBookDetails};
