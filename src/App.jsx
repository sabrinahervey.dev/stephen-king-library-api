//import des composants BookList et BookDetails
import BookList from "./components/BookList";
import BookDetails from "./components/BookDetails";

// Fonction principale App
function App() {
  return (
    <>
      <BookList />
      <BookDetails />
    </>
  );
}
// Exportation de la fonction App
export default App;
