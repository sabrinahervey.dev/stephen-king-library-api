import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
//import des éléments depuis react-router-dom
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import BookList from './components/BookList.jsx';
import BookDetails from './components/BookDetails.jsx';

// Création d'un router pour gérer les routes de l'application
const router = createBrowserRouter ([
  // Déclaration de la route principale
  {
    path: "/",
    element: <BookList />
  },
  { //declare la route et prend un paramatre bookId automatiquement (sera recup par useParams)
    path: "/BookDetails/works/:bookId",
    element: <BookDetails />
  }
]);

// Rendu du composant RouterProvider avec le router créé
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
